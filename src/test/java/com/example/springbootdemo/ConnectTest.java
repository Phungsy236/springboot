package com.example.springbootdemo;

import com.example.springbootdemo.endpoints.Connect;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(Connect.class)
//class ConnectTest {
//    @Autowired
//    MockMvc mockMvc;
//
//    @Test
//    void shouldResponseHello() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders
//                        .get("/Test/")
//                        .contentType(MediaType.APPLICATION_JSON)
//                ).andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.content().string("hello"))
//        ;
//    }
//
//}
