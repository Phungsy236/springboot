package com.example.springbootdemo.services;

import com.example.springbootdemo.models.ErrorResponse;
import com.example.springbootdemo.models.appModels.User;
import com.example.springbootdemo.models.UserSecurity;
import com.example.springbootdemo.repos.UserRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Log4j2
@Component
@Transactional
public class UserSecurityService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String authorName) throws UsernameNotFoundException {
        User user = userRepo.findUserByName(authorName);
        if (user == null) {
            throw new UsernameNotFoundException(authorName);
        }
        return new UserSecurity(user);
    }

    public ResponseEntity register(User user){
        try{
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            User userCreated = userRepo.save(user);
            return new ResponseEntity( userCreated, HttpStatus.CREATED);
        }catch (Exception e){
            log.error(e);
            return new ResponseEntity(new ErrorResponse(500), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


}
