package com.example.springbootdemo.handleException;

public class MessageError {
    public static final String NAME_REQUEIRD = "Thiếu tên";
    public static final String PASS_REQUEIRD = "Thiếu mật khẩu";
    public static final String SHORT_PASS = "Độ dài mật khẩu tối thiểu trên 6 ký tự";
    public static final String WRONG_PHONE_FORMAT = "Số điện thoại không đúng định dạng";
    public static final String WRONG_EMAIL_FORMAT = "Email không đúng định dạng";
}
