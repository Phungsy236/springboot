package com.example.springbootdemo.models.appModels;

import com.example.springbootdemo.handleException.MessageError;
import com.example.springbootdemo.models.BaseModel;
import com.example.springbootdemo.utils.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "user")

public class User extends BaseModel {

    @NotBlank(message = MessageError.NAME_REQUEIRD)
    @Column(nullable = false , unique = true)
    private String name;

//    @NotBlank(message = MessageError.PASS_REQUEIRD)
//    @Min(value = 6 , message = MessageError.SHORT_PASS)
    private String password;

    @Pattern(regexp = Constants.VN_PHONE_REGEX , message = MessageError.WRONG_PHONE_FORMAT)
    private String phone;

    @Email(message = MessageError.WRONG_EMAIL_FORMAT)
    private String email;

    @Column(name = "date_of_birth")
    private LocalDateTime dateOfBirth;

    private String address;

}
