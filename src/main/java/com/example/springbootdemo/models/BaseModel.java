package com.example.springbootdemo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@MappedSuperclass
public class BaseModel {
    @Id
    @GeneratedValue
    @Setter(onMethod = @__(@JsonIgnore)) // when post Grade entity Jackson ignore id property
    @Getter(onMethod = @__(@JsonProperty))
    private long id;
    @Column(name = "created_at" , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime created_at;

    @Column(name = "updated_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP On update CURRENT_TIMESTAMP")
    private LocalDateTime updated_at;
}
