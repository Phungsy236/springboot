package com.example.springbootdemo.models;

import lombok.Data;

import java.util.Map;

@Data
//@AllArgsConstructor
public class ErrorResponse {
    private int errorCode ;
    private Map<String , String> detail;

    public ErrorResponse(int errorCode, Map<String, String> detail) {
        this.errorCode = errorCode;
        this.detail = detail;
    }
    public ErrorResponse(int errorCode) {
        this.errorCode = errorCode;

    }


}
