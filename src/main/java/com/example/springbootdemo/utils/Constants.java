package com.example.springbootdemo.utils;

public class Constants {
    public static final String VN_PHONE_REGEX = "/(84|0[3|5|7|8|9])+([0-9]{8})\b/g";
    public static final int SERVER_ERROR_CODE = 10;
    public static final int BAD_REQUEST = 11;
    public static final int NOT_FOUND = 12;
    public static final int INVALIDATE = 13;
    public static final int DATABASE_ERROR = 14;
    public static final int DATABASE_CONSTRAINT_ERROR = 141;
}
