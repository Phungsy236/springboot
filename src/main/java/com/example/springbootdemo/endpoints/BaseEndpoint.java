package com.example.springbootdemo.endpoints;

import com.example.springbootdemo.models.ErrorResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

@Log4j2
public class BaseEndpoint<E, R extends JpaRepository<E, Long>> {
    R repo;
    public BaseEndpoint(R repo) {
        this.repo = repo;
    }

    @GetMapping("/{id}")
    public ResponseEntity<E> getDetail(@PathVariable(value = "id") long id) {
        return new ResponseEntity<>(repo.findById(id).orElseThrow(() -> null), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Object> getFull() {
        try {
            return new ResponseEntity<>(repo.findAll(), HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return new ResponseEntity(new ErrorResponse(500) , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    public ResponseEntity create(@Valid @RequestBody E entity) throws NoSuchAlgorithmException {
        try {
            return new ResponseEntity(repo.save(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            log.error(e);
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable(value = "id") long id,@Valid @RequestBody E entity) {
        try {
            E oldObj = (E) repo.findById(id);
            BeanUtils.copyProperties(entity, oldObj);
            return new ResponseEntity(repo.save(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            log.error(e);
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);

        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") long id) {
        E e = (E) repo.findById(id);
        if (!Objects.isNull(e)) {
            repo.deleteById(id);
            return new ResponseEntity(e, HttpStatus.OK);
        } else return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/page/{pageId}")
    public Page<E> getAndPagination(@PathVariable(value = "pageId") int pageId) {
        final int default_Size = 3;
        Pageable pageable = PageRequest.of(pageId - 1, default_Size);
        return repo.findAll(pageable);
    }
}
