package com.example.springbootdemo.endpoints;

import com.example.springbootdemo.config.JwtConfig;
import com.example.springbootdemo.models.LoginRequest;
import com.example.springbootdemo.models.UserSecurity;
import com.example.springbootdemo.models.appModels.User;
import com.example.springbootdemo.services.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/Authen")
public class Authen {

    @Autowired
    JwtConfig jwtConfig;

    @Autowired
    UserSecurityService userSecurityService;
    @Autowired
    AuthenticationManager authenticationManager;
    @PostMapping("/login")
    public ResponseEntity<Object> login(@Valid @RequestBody LoginRequest loginRequest){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getName(),
                        loginRequest.getPass()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtConfig.generateToken((UserSecurity) authentication.getPrincipal());
        Map<String , String> response = new HashMap<>();
        response.put("token" , jwt);
        return new ResponseEntity<>(response , HttpStatus.OK);
    }
    @PostMapping("/register")
    public ResponseEntity<Object> register(@Valid @RequestBody User user){
        return userSecurityService.register(user);
    }
}
