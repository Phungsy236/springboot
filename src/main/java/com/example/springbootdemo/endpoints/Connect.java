package com.example.springbootdemo.endpoints;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Test")
public class Connect {
    @GetMapping("/")
    public String Test(){
        return "hello";
    }
}
