package com.example.springbootdemo.repos;

import com.example.springbootdemo.models.appModels.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    User findUserByName(String name );
}
