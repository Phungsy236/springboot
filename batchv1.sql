-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (19);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_wiki`
--

DROP TABLE IF EXISTS `people_wiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `people_wiki` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `people_name` varchar(255) NOT NULL,
  `author_id` bigint NOT NULL,
  `description` text NOT NULL,
  `aliases` varchar(100) DEFAULT NULL,
  `weakness` varchar(255) DEFAULT NULL,
  `hobby` varchar(255) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_public` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_name_uindex` (`people_name`),
  KEY `blog_author_id_fk` (`author_id`),
  CONSTRAINT `blog_author_id_fk` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_wiki`
--

LOCK TABLES `people_wiki` WRITE;
/*!40000 ALTER TABLE `people_wiki` DISABLE KEYS */;
INSERT INTO `people_wiki` VALUES (1,'Đặng Quý Long',1,'Phó tổng giám đốc STC VNPT','a Looong','Béo',NULL,80,169,NULL,'2021-08-18 11:08:54','2021-08-18 11:28:46',1);
/*!40000 ALTER TABLE `people_wiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `react`
--

DROP TABLE IF EXISTS `react`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `react` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `react_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `react`
--

LOCK TABLES `react` WRITE;
/*!40000 ALTER TABLE `react` DISABLE KEYS */;
INSERT INTO `react` VALUES (1,'like','yêu thích','2021-08-18 11:51:44','2021-08-18 04:51:44'),(2,'dislike',NULL,'2021-08-18 11:51:44','2021-08-18 04:51:44'),(3,'haha',NULL,'2021-08-18 11:51:44','2021-08-18 04:51:44'),(4,'angry',NULL,'2021-08-18 11:51:44','2021-08-18 04:51:44'),(5,'favourite',NULL,'2021-08-18 11:51:44','2021-08-18 04:51:44');
/*!40000 ALTER TABLE `react` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_or6k6jmywerxbme223c988bmg` (`name`),
  UNIQUE KEY `author_email_uindex` (`email`),
  UNIQUE KEY `author_phone_uindex` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Xuân Diệu','','dieu@gmail.com','1916-02-02',NULL,NULL,'2021-08-18 11:31:30','2021-08-18 04:31:30'),(10,'ok','$2a$10$PbGkb4tCD1gLafy4b00/UOujkz9p3JLtqYKYvLzPpzTEqTq3uU/6S',NULL,NULL,NULL,NULL,NULL,NULL),(11,'a','$2a$10$PbGkb4tCD1gLafy4b00/UOujkz9p3JLtqYKYvLzPpzTEqTq3uU/6S',NULL,NULL,NULL,NULL,'2021-08-18 13:31:00','2021-08-18 06:31:00'),(13,'ok1','$2a$10$sF6I348eNVdNJ8PYd2giPewOqSQFsAS58.9Zjcv7mo5OS8RFBNkhK',NULL,NULL,NULL,NULL,NULL,NULL),(15,'ok2','$2a$10$Ry/TusAHoFMKO8KrE58u9uYdbRWazf24pE4ZS7mMBHaNM5qWoPtga',NULL,NULL,NULL,NULL,NULL,NULL),(16,'ok3','$2a$10$Ptq8F/Pn0R6at.bfkrw2i.laR.Q00X5nMF5gihbE0fc.Tat9EExfq',NULL,NULL,NULL,NULL,NULL,NULL),(17,'ok4','$2a$10$2nt3QTZ4u8otfy2cqsVD4O4eItGmFUXLyakT2Q49OjzEF3kj8HOgK',NULL,NULL,NULL,NULL,NULL,NULL),(18,'ok6','$2a$10$GwVGXCCVpGCPduIApM4I0e1E/jzNC6tn1Ce7G/VlZlCxz7C3afEk2',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_react`
--

DROP TABLE IF EXISTS `wiki_react`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wiki_react` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `wiki_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `react_id` int NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `wiki_react_react_id_fk` (`react_id`),
  KEY `wiki_react_wiki_id_fk` (`wiki_id`),
  KEY `wiki_user` (`user_id`),
  CONSTRAINT `wiki_react_react_id_fk` FOREIGN KEY (`react_id`) REFERENCES `react` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wiki_react_wiki_id_fk` FOREIGN KEY (`wiki_id`) REFERENCES `people_wiki` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wiki_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_react`
--

LOCK TABLES `wiki_react` WRITE;
/*!40000 ALTER TABLE `wiki_react` DISABLE KEYS */;
INSERT INTO `wiki_react` VALUES (1,1,1,1,'2021-08-18 11:36:05');
/*!40000 ALTER TABLE `wiki_react` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-18 14:09:21
